package graph;

public interface Edge {

	public abstract boolean incident(String x);

	public abstract String vertexOne();

	public abstract String vertexTwo();
	
	@Override
	public boolean equals(Object other);
	
}