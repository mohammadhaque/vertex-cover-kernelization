package graph;

import java.util.List;

public interface Graph {

	public void addEdge(Edge e);

	public void addEdge(String u, String v);

	public void addVertex(String v);

	public int degree(String u);

	public List<Edge> edges();

	@Override
	public boolean equals(Object other);

	public List<Edge> incidentEdges(String v);

	public boolean isAdjacent(String u, String v);
	
	public List<String> neighbours(String u);

	public List<Edge> nonEdges();

	public List<String> notAdjacent(String u);

	public int numEdges();

	public int numVertices();

	public void removeEdge(Edge e);

	public void removeEdge(String u, String v);
	
	public void removeVertex(String u);

	public Graph textListToGraph(String text);

	public List<String> vertices();

	public void setAdjacencyMatrix(boolean[][] matrix);

}
