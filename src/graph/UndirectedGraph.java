package graph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class UndirectedGraph implements Graph {

	private ArrayList<String> vertices;
	private Map<Integer, List<String>> neighbours;
	private int n = 0, e = 0;

	public UndirectedGraph() {
		this.vertices = new ArrayList<String>();
		this.neighbours = new HashMap<Integer, List<String>>();
	}

	public UndirectedGraph(Graph g) {

		this.vertices = new ArrayList<String>();
		this.neighbours = new HashMap<Integer, List<String>>();

		for (String v : g.vertices())
			this.addVertex(v);

		for (Edge e : g.edges())
			this.addEdge(e);

	}

	@Override
	public void addEdge(Edge e) {

		this.addEdge(e.vertexOne(), e.vertexTwo());

	}

	@Override
	public void addEdge(String u, String v) {

		if (!u.equals(v)) {

			if (!this.vertices.contains(u)) {
				this.addVertex(u);
			}

			if (!this.vertices.contains(v)) {
				this.addVertex(v);
			}

			List<String> uNeighbours = this.neighbours.get(this.vertices
					.indexOf(u));
			if (uNeighbours == null) {
				uNeighbours = new ArrayList<String>();
			}
			if (!uNeighbours.contains(v)) {
				uNeighbours.add(v);
				this.e++; // Note that the edge count will fail if the graph is
							// not
							// properly maintained.
			}

			List<String> vNeighbours = this.neighbours.get(this.vertices
					.indexOf(v));
			if (vNeighbours == null) {
				vNeighbours = new ArrayList<String>();
			}
			if (!vNeighbours.contains(u)) {
				vNeighbours.add(u);
			}

			this.neighbours.put(this.vertices.indexOf(u), uNeighbours);
			this.neighbours.put(this.vertices.indexOf(v), vNeighbours);

		}

	}

	@Override
	public void addVertex(String v) {
		if (!this.vertices.contains(v)) {
			this.vertices.add(v);
			this.neighbours.put(this.vertices.indexOf(v),
					new ArrayList<String>());
			this.n++;
		}
	}

	@Override
	public int degree(String u) {
		return this.neighbours(u).size();
	}

	@Override
	public List<Edge> edges() {

		List<Edge> edges = new ArrayList<Edge>();

		for (String v : this.vertices) {
			for (String u : this.neighbours(v)) {
				UndirectedEdge e = new UndirectedEdge(v, u);
				if (!edges.contains(e)) {
					edges.add(e);
				}
			}
		}

		return edges;

	}

	@Override
	public boolean equals(Object other) {

		if (other == null)
			return false;

		if (other.getClass() != this.getClass())
			return false;

		UndirectedGraph g = (UndirectedGraph) other;

		if (g.numVertices() != this.numVertices()
				|| g.numEdges() != this.numEdges())
			return false;

		for (String v : this.vertices()) {

			if (!g.vertices().contains(v))
				return false;

			if (this.neighbours(v).size() != g.neighbours(v).size())
				return false;

			for (String u : this.neighbours(v)) {

				if (!g.neighbours(v).contains(u))
					return false;

			}

		}

		return true;
	}

	@Override
	public List<Edge> incidentEdges(String v) {
		List<Edge> edges = new ArrayList<Edge>();

		for (String u : this.neighbours(v)) {
			UndirectedEdge e = new UndirectedEdge(v, u);
			edges.add(e);
		}

		return edges;
	}

	@Override
	public boolean isAdjacent(String u, String v) {
		List<String> neighbours = this.neighbours.get(this.vertices.indexOf(u));

		if (neighbours != null)
			return neighbours.contains(v);

		return false;
	}

	@Override
	public List<String> neighbours(String u) {
		return new ArrayList<String>(this.neighbours.get(this.vertices
				.indexOf(u)));
	}

	@Override
	public List<Edge> nonEdges() {

		List<Edge> nonEdges = new ArrayList<Edge>();

		for (String u : this.vertices()) {
			for (String v : this.notAdjacent(u)) {
				Edge e = new UndirectedEdge(u, v);

				if (!nonEdges.contains(e)) {
					nonEdges.add(e);
				}
			}
		}
		return nonEdges;
	}

	@Override
	public List<String> notAdjacent(String u) {

		List<String> notAdjacent = new ArrayList<String>(this.n
				- this.degree(u));

		for (String v : this.vertices()) {
			if (!this.isAdjacent(u, v)) {
				notAdjacent.add(v);
			}
		}

		notAdjacent.remove(u);

		return notAdjacent;

	}

	@Override
	public int numEdges() {
		return this.e;
	}

	@Override
	public int numVertices() {
		return this.n;
	}

	@Override
	public void removeEdge(Edge e) {

		this.removeEdge(e.vertexOne(), e.vertexTwo());

	}

	@Override
	public void removeEdge(String u, String v) {

		int uIndex = this.vertices.indexOf(u);
		int vIndex = this.vertices.indexOf(v);

		List<String> uEdges = this.safe(this.neighbours.get(uIndex));
		List<String> vEdges = this.safe(this.neighbours.get(vIndex));

		if (uEdges.remove(v))
			this.e--;
		vEdges.remove(u);

		this.neighbours.put(uIndex, uEdges);
		this.neighbours.put(vIndex, vEdges);

	}

	@Override
	public void removeVertex(String u) {

		int uIndex = this.vertices.indexOf(u);

		if (uIndex != -1) {

			for (String v : this.vertices) {
				this.removeEdge(u, v);
			}

			for (int i = uIndex + 1; i < this.vertices.size(); i++) {

				List<String> neighbourhood = this.safe(this.neighbours.get(i));
				this.neighbours.put(i - 1, neighbourhood);

			}

			this.neighbours.remove(this.neighbours.size());
			if (this.vertices.remove(uIndex) != null)
				this.n--;

		}

	}

	private <T> List<T> safe(List<T> list) {

		return list == null ? Collections.<T> emptyList() : list;
	}

	@Override
	public Graph textListToGraph(String text) {

		String[] rawAdjacencyLists = text.split("[\\(\\)]");
		ArrayList<String> adjacencyLists = new ArrayList<String>();

		String[] labels = text.split("[\\(\\)\\s+\\,\\[\\]\\']+");

		Graph graph = new UndirectedGraph();

		Set<String> labelSet = new HashSet<String>();

		for (String s : labels) {
			if (!s.equals("")) {
				labelSet.add(s);
			}
		}

		for (String s : labelSet)
			graph.addVertex(s);

		for (String s : rawAdjacencyLists) {
			if (!(s.trim().equals("[") || s.trim().equals(","))) {
				adjacencyLists.add(s.trim());
			}
		}

		for (String list : adjacencyLists) {
			String[] tokens = list.split("[\\s+\\,\\[\\]\\']+");
			if (tokens.length > 0) {
				tokens = Arrays.copyOfRange(tokens, 1, tokens.length);

				String v = new String(tokens[0]);

				for (int i = 1; i < tokens.length; i++) {

					String u = new String(tokens[i]);
					graph.addEdge(v, u);

				}

			}

		}

		return graph;

	}

	@Override
	public String toString() {

		String output = "";

		output += "Graph: ";
		List<String> neighbours = new ArrayList<String>();
		for (String v : this.vertices()) {
			neighbours.add("(" + v + " : "
					+ this.neighbours.get(this.vertices.indexOf(v)) + ")");
		}
		output += neighbours.toString();

		return output;
	}

	@Override
	public List<String> vertices() {
		return new ArrayList<String>(this.vertices);
	}

	@Override
	public void setAdjacencyMatrix(boolean[][] matrix) {

		List<Edge> edges = this.edges();

		for (Edge e : edges) {
			this.removeEdge(e);
		}

		for (int i = 0; i < matrix.length; i++) {
			for (int j = i + 1; j < matrix.length; j++) {
				if (matrix[i][j]) {
					this.addEdge(this.vertices.get(i), this.vertices.get(j));
				}
			}
		}

	}

}
