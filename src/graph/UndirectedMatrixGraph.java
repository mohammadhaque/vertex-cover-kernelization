package graph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class UndirectedMatrixGraph implements Graph {

	private List<String> vertexLabels;
	private boolean[][] adjacency;
	private int numVertices;
	private int numEdges;
	private Map<String, Integer> vertexIndices;

	public UndirectedMatrixGraph() {
		super();

		this.vertexLabels = new ArrayList<String>();
		this.adjacency = new boolean[0][0];
		this.numVertices = 0;
		this.numEdges = 0;
		this.vertexIndices = new HashMap<String, Integer>();
	}

	public UndirectedMatrixGraph(Graph g) {

		this.vertexLabels = new ArrayList<String>();
		this.adjacency = new boolean[0][0];
		this.numVertices = 0;
		this.numEdges = 0;
		this.vertexIndices = new HashMap<String, Integer>();

		for (String v : g.vertices())
			this.addVertex(v);

		for (Edge e : g.edges()) {
			this.addEdge(e);
		}

	}

	@Override
	public void addEdge(Edge e) {
		this.addEdge(e.vertexOne(), e.vertexTwo());
	}

	@Override
	public void addEdge(String u, String v) {

		Integer i = this.vertexIndices.get(u);
		Integer j = this.vertexIndices.get(v);

		if (i == null) {
			this.addVertex(u);
			i = this.vertexIndices.get(u);
		}

		if (j == null) {
			this.addVertex(v);
			j = this.vertexIndices.get(v);
		}

		if (i != j) {
			this.adjacency[i][j] = true;
			this.adjacency[j][i] = true;

			this.numEdges++;
		}
	}

	@Override
	public void addVertex(String v) {

		if (!vertexLabels.contains(v)) {

			boolean[][] newAdjacency = new boolean[this.numVertices + 1][this.numVertices + 1];
			for (int i = 0; i < this.numVertices; i++) {
				for (int j = i + 1; j < this.numVertices; j++) {
					newAdjacency[i][j] = this.adjacency[i][j];
					newAdjacency[j][i] = this.adjacency[j][i];
				}
			}

			this.adjacency = newAdjacency;
			this.vertexLabels.add(v);
			this.vertexIndices.put(v, this.numVertices);
			this.numVertices++;
		}

	}

	@Override
	public int degree(String u) {
		return this.neighbours(u).size();
	}

	@Override
	public List<Edge> edges() {
		List<Edge> edges = new ArrayList<Edge>();

		for (String v : this.vertexLabels) {
			for (String u : this.neighbours(v)) {
				UndirectedEdge e = new UndirectedEdge(v, u);
				if (!edges.contains(e)) {
					edges.add(e);
				}
			}
		}

		return edges;
	}

	@Override
	public boolean equals(Object other) {

		if (other == null)
			return false;

		if (other.getClass() != this.getClass())
			return false;

		UndirectedGraph g = (UndirectedGraph) other;

		if (g.numVertices() != this.numVertices()
				|| g.numEdges() != this.numEdges())
			return false;

		for (String v : this.vertices()) {

			if (!g.vertices().contains(v))
				return false;

			if (this.neighbours(v).size() != g.neighbours(v).size())
				return false;

			for (String u : this.neighbours(v)) {

				if (!g.neighbours(v).contains(u))
					return false;

			}

		}

		return true;
	}

	@Override
	public List<Edge> incidentEdges(String v) {

		List<Edge> incident = new ArrayList<Edge>();

		for (String u : this.neighbours(v)) {
			incident.add(new UndirectedEdge(v, u));
		}

		return incident;
	}

	@Override
	public boolean isAdjacent(String u, String v) {

		Integer i = this.vertexIndices.get(u);
		Integer j = this.vertexIndices.get(v);

		if (i != null && j != null) {
			return adjacency[i][j];
		}

		return false;
	}

	@Override
	public List<String> neighbours(String u) {

		Integer i = this.vertexIndices.get(u);

		if (i != null) {

			List<String> neighbours = new ArrayList<String>();

			for (int j = 0; j < this.numVertices; j++) {
				if (adjacency[i][j])
					neighbours.add(this.vertexLabels.get(j));
			}

			return neighbours;

		}

		return Collections.<String> emptyList();
	}

	@Override
	public List<Edge> nonEdges() {

		List<Edge> nonEdges = new ArrayList<Edge>();

		for (int i = 0; i < this.numVertices; i++) {
			for (int j = i + 1; j < this.numVertices; j++) {
				if (!this.adjacency[i][j]) {
					nonEdges.add(new UndirectedEdge(this.vertexLabels.get(i),
							this.vertexLabels.get(j)));
				}
			}
		}

		return nonEdges;
	}

	@Override
	public List<String> notAdjacent(String u) {

		Integer i = this.vertexIndices.get(u);

		if (i != null) {

			List<String> nonNeighbours = new ArrayList<String>();

			for (int j = 0; j < this.numVertices; j++) {
				if (!adjacency[i][j] && i != j)
					nonNeighbours.add(this.vertexLabels.get(j));
			}

			return nonNeighbours;

		}

		return Collections.<String> emptyList();
	}

	@Override
	public int numEdges() {
		return this.numEdges;
	}

	@Override
	public int numVertices() {
		return this.numVertices;
	}

	@Override
	public void removeEdge(Edge e) {

		this.removeEdge(e.vertexOne(), e.vertexTwo());

	}

	@Override
	public void removeEdge(String u, String v) {

		Integer i = this.vertexIndices.get(u);
		Integer j = this.vertexIndices.get(v);

		if (i != null && j != null && adjacency[i][j]) {

			adjacency[i][j] = false;
			adjacency[j][i] = false;
			this.numEdges--;
		}

	}

	@Override
	public void removeVertex(String u) {

		Integer i = this.vertexIndices.get(u);

		if (i != null) {

			boolean[][] newAdjacency = new boolean[this.numVertices - 1][this.numVertices - 1];
			for (int k = 0; k < this.numVertices(); k++) {
				for (int j = 0; j < this.numVertices; j++) {
					if (k < i) {
						if (j < i) {
							newAdjacency[k][j] = this.adjacency[k][j];
						} else if (j > i) {
							newAdjacency[k][j - 1] = this.adjacency[k][j];
						}
					} else if (k > i) {
						if (j < i) {
							newAdjacency[k - 1][j] = this.adjacency[k][j];
						} else if (j > i) {
							newAdjacency[k - 1][j - 1] = this.adjacency[k][j];
						}
					}
				}
			}

			int d = this.degree(u);
			this.adjacency = newAdjacency;
			this.vertexLabels.remove(i);
			this.vertexIndices.remove(u);
			this.numEdges -= d;
			this.numVertices--;
		}

	}

	@Override
	public Graph textListToGraph(String text) {

		String[] rawAdjacencyLists = text.split("[\\(\\)]");
		ArrayList<String> adjacencyLists = new ArrayList<String>();

		String[] labels = text.split("[\\(\\)\\s+\\,\\[\\]\\']+");

		Graph graph = new UndirectedGraph();

		Set<String> labelSet = new HashSet<String>();

		for (String s : labels) {
			if (!s.equals("")) {
				labelSet.add(s);
			}
		}

		for (String s : labelSet)
			graph.addVertex(s);

		for (String s : rawAdjacencyLists) {
			if (!(s.trim().equals("[") || s.trim().equals(","))) {
				adjacencyLists.add(s.trim());
			}
		}

		for (String list : adjacencyLists) {
			String[] tokens = list.split("[\\s+\\,\\[\\]\\']+");
			if (tokens.length > 0) {
				tokens = Arrays.copyOfRange(tokens, 1, tokens.length);

				String v = new String(tokens[0]);

				for (int i = 1; i < tokens.length; i++) {

					String u = new String(tokens[i]);
					graph.addEdge(v, u);

				}

			}

		}

		return graph;

	}

	@Override
	public String toString() {

		String output = "";

		output += "Graph: ";
		List<String> neighbours = new ArrayList<String>();
		for (String v : this.vertices()) {
			neighbours.add("(" + v + " : "
					+ this.neighbours(v) + ")");
		}
		output += neighbours.toString();

		return output;
	}
	
	@Override
	public List<String> vertices() {
		List<String> vertices = new ArrayList<String>(this.vertexLabels);

		return vertices;
	}
	
	/**
	 * Warning: this method does no real sanity checking
	 * on the input at the moment.
	 * 
	 * @param matrix
	 */
	@Override
	public void setAdjacencyMatrix(boolean[][] matrix){
		
		if (this.numVertices > 0 && matrix.length == this.numVertices && matrix[0].length == this.numVertices){
			
			int numEdges = 0;
			
			for (int i = 0; i < matrix.length; i++){
				for (int j = 0; j < matrix[i].length; j++){
					this.adjacency[i][j] = matrix[i][j];
					if (matrix[i][j]){
						numEdges++;
					}
				}
			}
			
			this.numEdges = numEdges/2;
		}
		
	}

}
