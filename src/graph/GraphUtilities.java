package graph;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;

import org.princeton.Libraries.Queue;


public class GraphUtilities {

    //mohammad: generate an induced graph for a list of vertices
    public static Graph generateInducedGraph(Graph G2, List<String> V) {
        Graph inducedG = GraphUtilities.copy(G2);
        List<String> G2V = new ArrayList<String>();
        G2V = G2.vertices();
        G2V.removeAll(V);   // keeping the vertices to remove

        // remove vertices which are not induced by f(V1)
        for (String vertex : G2V) {
            inducedG.removeVertex(vertex);
        }
        return inducedG;
    }

    //mohammad: generate an induced graph for a list of vertices
    public static Graph removeGraphVertices(Graph G, List<String> V) {
        Graph newGraph = GraphUtilities.copy(G);
        List<String> GV = new ArrayList<String>();
        GV = G.vertices();
        GV.removeAll(V);   // keeping the vertices to remove

        // remove vertices which are not induced by f(V1)
        for (String vertex : GV) {
            newGraph.removeVertex(vertex);
        }
        return newGraph;
    }

    // mohammad: Connected Components
    private static boolean[] visited;
    private static int[] id;
    private static int[] size;
    private static int count = 0;
    private static List<String> vertices;

    public static Graph getLargestConnectedCommonSubgraph(Graph G) {
        calculateConnectedComponents(G);

        // number of connected components
        int m = count;
        //System.out.println(m + " components");

        // compute list of vertices in each connected component
        Queue<Integer>[] components = (Queue<Integer>[]) new Queue[m];
        for (int i = 0; i < m; i++) {
            components[i] = new Queue<Integer>();
        }
        for (int v = 0; v < G.numVertices(); v++) {
            components[id[v]].enqueue(v);
        }

        // print results
        int max = components[0].size();
        int maxIdx = 0;
        for (int i = 0; i < m; i++) {
            //System.out.print("Component [" + (i + 1) + "] (" + components[i].size() + ") Members: ");
            if (max < components[i].size()) {
                max = components[i].size();
                maxIdx = i;

            }

            /*else {
                for (int v : components[i]) {
                    //System.out.print(vertices.get(v) + " ");
                }
            }
            //System.out.println();
             */
        }

        //System.out.println("Max Component " + max + " is at: " + maxIdx);
        List<String> lccsVertices = new ArrayList<String>();
        for (int v : components[maxIdx]) {
            //System.out.print(vertices.get(v) + " ");
            lccsVertices.add(vertices.get(v));
        }
        //System.out.println("\n"+lccsVertices.toString());
        Graph lccs = generateInducedGraph(G, lccsVertices);
        return lccs;
    }

    // Function: calculateConnectedComponents
    // Find the connecetd componet of the graph using DFS
    private static void calculateConnectedComponents(Graph G) {
        //count=0;
        visited = new boolean[G.numVertices()];
        vertices = G.vertices();
        id = new int[G.numVertices()];
        size = new int[G.numVertices()];

        for (int v = 0; v < G.numVertices(); v++) {
            if (!visited[v]) {
                dfs(G, v);
                count++;
            }
        }
    }

    // Function: dfs
    // Depth First Traversal or DFS for a Graph
    private static void dfs(Graph G, int v) {
        visited[v] = true;
        id[v] = count;
        size[count]++;
        String vertex = vertices.get(v);
        List<String> neighbours = G.neighbours(vertex);

        for (String w : neighbours) {
            int idx = vertices.indexOf(w);
            if (!visited[idx]) {
                dfs(G, idx);
            }
        }
    }

    public static boolean isIsomorphic(Graph g, Graph h,
            Map<String, String> mapping) {

        int gMaxDegree = 0;
        int hMaxDegree = 0;

        Set<String> gMatched = mapping.keySet();
        Collection<String> hMatched = mapping.values();

        if (gMatched.size() == g.numVertices()
                && hMatched.size() == h.numVertices()) {
            return true;
        }

        for (String v : g.vertices()) {
            if (!gMatched.contains(v)) {
                gMaxDegree = g.degree(v) > gMaxDegree ? g.degree(v)
                        : gMaxDegree;
            }
        }

        for (String v : h.vertices()) {
            if (!hMatched.contains(v)) {
                hMaxDegree = h.degree(v) > hMaxDegree ? h.degree(v)
                        : hMaxDegree;
            }
        }

        if (gMaxDegree != hMaxDegree) {
            return false;
        }

        int[] gDegreeCounts = new int[gMaxDegree + 1];
        int[] hDegreeCounts = new int[hMaxDegree + 1];

        List<String> gMaxDegreeVertices = new ArrayList<String>();
        List<String> hMaxDegreeVertices = new ArrayList<String>();

        for (String v : g.vertices()) {
            if (g.degree(v) == gMaxDegree && !gMatched.contains(v)) {
                gDegreeCounts[g.degree(v)]++;
                gMaxDegreeVertices.add(v);
            }
        }

        for (String v : h.vertices()) {
            if (h.degree(v) == hMaxDegree && !hMatched.contains(v)) {
                hDegreeCounts[h.degree(v)]++;
                hMaxDegreeVertices.add(v);
            }
        }

        if (!Arrays.equals(gDegreeCounts, hDegreeCounts)) {
            return false;
        }

        if (gDegreeCounts[0] > 0) {
            List<String> gZeroDegree = new ArrayList<String>();
            List<String> hZeroDegree = new ArrayList<String>();

            for (String v : g.vertices()) {
                if (g.degree(v) == 0) {
                    gZeroDegree.add(v);
                }
            }

            for (String v : h.vertices()) {
                if (h.degree(v) == 0) {
                    hZeroDegree.add(v);
                }
            }

            for (int i = 0; i < gZeroDegree.size(); i++) {
                mapping.put(gZeroDegree.get(i), hZeroDegree.get(i));
            }
        }

        boolean checkNext = false;

        for (String u : gMaxDegreeVertices) {
            for (String v : hMaxDegreeVertices) {

                for (String n : g.neighbours(u)) {

                    if (gMatched.contains(n)) {
                        if (!h.isAdjacent(v, mapping.get(n))) {
                            return false;
                        }
                    }

                }

                Map<String, String> newMapping = new HashMap<String, String>(
                        mapping);
                newMapping.put(u, v);
                checkNext |= isIsomorphic(g, h, newMapping);

            }
        }

        return checkNext;
    }

    public static Graph copy(Graph g) {
        try {
            if (g == null) {
                return null;
            }

            Class<? extends Graph> gClass = g.getClass();

            Graph gPrime = gClass.newInstance();

            for (Edge e : g.edges()) {
                gPrime.addEdge(e);
            }

            return gPrime;

        } catch (InstantiationException | IllegalAccessException e) {
            return null;
        }
    }

    public static boolean isIsomorphic(Graph g, Graph h) {

        if (g.numVertices() != h.numVertices() || g.numEdges() != h.numEdges()) {
            return false;
        }

        return isIsomorphic(g, h, new HashMap<String, String>());
    }

}
