package graph;

public class UndirectedEdge implements Edge {

	String u, v;

	public UndirectedEdge(String u, String v) {

		this.u = u;
		this.v = v;

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((u == null) ? 0 : u.hashCode());
		result = prime * result + ((v == null) ? 0 : v.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (obj == this)
			return true;

		if (!(obj.getClass() == this.getClass()))
			return false;

		Edge e = (Edge) obj;

		return (this.vertexOne().equals(e.vertexOne()) && this.vertexTwo()
				.equals(e.vertexTwo()))
				|| (this.vertexOne().equals(e.vertexTwo()) && this.vertexTwo()
						.equals(e.vertexOne()));

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see graph.Edge#incident(java.lang.String)
	 */
	@Override
	public boolean incident(String x) {
		return x.compareTo(u) == 0 || x.compareTo(v) == 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see graph.Edge#vertexOne()
	 */
	@Override
	public String vertexOne() {
		return this.u;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see graph.Edge#vertexTwo()
	 */
	@Override
	public String vertexTwo() {
		return this.v;
	}
	
	@Override
	public String toString(){
		return "(" + this.vertexOne() + "," + this.vertexTwo() + ")";
	}

}
