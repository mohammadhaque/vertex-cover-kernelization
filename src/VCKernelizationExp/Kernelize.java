/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VCKernelizationExp;

import VertexCover.CPLEXSolver;
import VertexCover.CompleteNeighbourhoodRule;
import VertexCover.CrownRule;
import VertexCover.DegreeKRule;
import VertexCover.DegreeOneRule;
import VertexCover.DegreeTwoAdjacentRule;
import VertexCover.DegreeTwoNonAdjacentRule;
import VertexCover.DegreeZeroRule;
import VertexCover.GeneralFoldRule;
import VertexCover.LPRule;
import VertexCover.NoKCoverException;
import VertexCover.Pair;
import VertexCover.ReductionRule;
import VertexCover.Solver;
import VertexCover.StructionRule;
import VertexCover.VertexCoverUtils;
import graph.Graph;
import graph.GraphUtilities;
import graph.UndirectedGraph;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.paukov.combinatorics.Factory;
import org.paukov.combinatorics.Generator;
import org.paukov.combinatorics.ICombinatoricsVector;
import solvers.CPLEXExactSolver;

/**
 *
 * @author mnhaque
 */
public class Kernelize {

    public static int findK(Graph graph, VertexCoverUtils vcSolver,
            List<ReductionRule> rules, Solver solver) {

        int k = 0;
        boolean kFound = false;

        while (!kFound) {
            try {
                Pair<Graph, List<String>> reducedInstance = vcSolver.reduce(
                        graph, rules, k);
                vcSolver.solve(reducedInstance.fst(), k
                        - reducedInstance.snd().size(), solver);
                kFound = true;
            } catch (NoKCoverException e) {
                k++;
            }
        }
        return k;
    }

    public static int countSolutions(Graph graph, int k) {

        int numSolutions = 0;

        ICombinatoricsVector<String> vertices = Factory.createVector(graph.vertices());
        Generator<String> combinationGenerator = Factory.createSimpleCombinationGenerator(vertices,
                graph.numVertices() - k);

        if (k < graph.numVertices() / 2) {
            // Enumerate the solutions by generating
            // all sets of vertices _in_ the solution.
            for (ICombinatoricsVector<String> vertexCover : combinationGenerator) {

                Graph tempGraph = new UndirectedGraph(graph);
                for (String v : vertexCover) {
                    tempGraph.removeVertex(v);
                }

                if (tempGraph.numEdges() == 0) {
                    numSolutions++;
                }
            }
        } else {
            // Enumerate the solutions by generating
            // all sets of vertices _not_ in the solution.
            for (ICombinatoricsVector<String> independentSet : combinationGenerator) {

                List<String> vertexCover = graph.vertices();
                for (String v : independentSet) {
                    vertexCover.remove(v);
                }

                Graph tempGraph = new UndirectedGraph(graph);
                for (String v : vertexCover) {
                    tempGraph.removeVertex(v);
                }

                if (tempGraph.numEdges() == 0) {
                    numSolutions++;
                }
            }
        }
        return numSolutions;
    }

    public static void main(String[] args) throws IOException, NoKCoverException {
        // /Users/mnhaque/Vertex-Cover/All-N8/G8_k=5_sc=4_mask_Czvbo0001001110111101111000111100_base_20150915_163914.gml.list /Users/mnhaque/Vertex-Cover/Few-N9/G9_k=6_sc=2_mask_000110101111100111100011111011101110_base_20150819_143140.gml.list
        // Instantiate the Graph objects
        Graph currentGraph = new UndirectedGraph();

        // C:\Mohammad-PostDoc-Works\Vertex-Cover-Reduction-Rules\Execute\Data\All-N8\IrreducibleInstance_n=8_k=5_sc=4_mask_Czvbo0001001110111101111000111100_base_20150915_163914.gml.list C:\Mohammad-PostDoc-Works\Vertex-Cover-Reduction-Rules\Execute\Data\All-N9\IrreducibleInstance_n=9_k=6_sc=2_mask_000110101001111111100111011001111010_base_20150819_143125.gml.list
        //C:\Mohammad-PostDoc-Works\Dataset\ConnectedCohesion\dolphins\dolphins.list
//Load File Name from cmd arguments
        String fileOne = args[0];

// read graph from the .lst file
        BufferedReader readerOne = new BufferedReader(new FileReader(new File(fileOne)));
        String graphString = "";
        while (readerOne.ready()) {
            graphString += readerOne.readLine();
        }
        readerOne.close();

        currentGraph = currentGraph.textListToGraph(graphString);
        System.out.println("Graph: " + currentGraph.toString());
        System.out.println("#Vertices: " + currentGraph.numVertices() + "\tEdges:" + currentGraph.numEdges());

        VertexCoverUtils vc = new VertexCoverUtils();
        List<ReductionRule> rules = new ArrayList<ReductionRule>();

        rules.add(DegreeZeroRule.INSTANCE);
        rules.add(DegreeOneRule.INSTANCE);
        rules.add(DegreeKRule.INSTANCE);
        rules.add(DegreeTwoAdjacentRule.INSTANCE);
        rules.add(DegreeTwoNonAdjacentRule.INSTANCE);
        rules.add(CompleteNeighbourhoodRule.INSTANCE);
        rules.add(CrownRule.INSTANCE);
        rules.add(StructionRule.INSTANCE);
        rules.add(LPRule.INSTANCE);
        rules.add(GeneralFoldRule.INSTANCE);

        Solver solver = CPLEXSolver.INSTANCE;

        // call CPLEX to compute the VC
        solvers.Solver cpExact = CPLEXExactSolver.INSTANCE;
        List<String> vcK = cpExact.solve(currentGraph);
        int k = vcK.size();
        
        Pair<Graph, List<String>> reduced = vc.reduce(currentGraph, rules, k);
        //int numSolutions = countSolutions(currentGraph, k);

        if (!(reduced.fst().numVertices() < currentGraph.numVertices())) {
            System.out.println("NOT REDUCED: " + fileOne);
            System.out.println("k=" + k );

//            System.out.println("IrreducibleInstance with: k=" + k + ", Solution Count=" + numSolutions);
//            String trailer = "-k=" + k + "_sc=" + numSolutions + "-IRR.gml";
            System.out.println("IrreducibleInstance with: k=" + k);
            String trailer = "-k=" + k + "-IRR.gml";

//            String trailer = "-k=" + k + "_sc=" + numSolutions + "-IRR.gml";
            String irrOutFile = fileOne.replace(".list", trailer);
            File gmlOutput = new File(irrOutFile);
            FileWriter fw = new FileWriter(gmlOutput);
            List<String> solution = solver.solve(currentGraph, k);
            fw.write(VertexCoverUtils.graphAndSolutionToGMLString(currentGraph, solution));
            fw.close();
            System.out.println("IRR file written at: " + irrOutFile);
            System.out.println("NO");
        } else {
            System.out.println("REDUCED: " + fileOne);
            System.out.println("#VR: " + reduced.fst().numVertices());
            System.out.println("k=" + k );
//            System.out.println("k=" + k + ", Solution Count=" + numSolutions);
            System.out.println("YES");
        }
    }

}
