package VertexCover;

import ilog.concert.IloException;
import ilog.cplex.IloCplex;

/**
 * Wrapper class to ensure that only a single instance of IloCplex is created in
 * the entire run. Otherwise, memory leaks ahoy!
 * 
 * @author Luke Mathieson
 *
 */
public class CPLEXSingleton {

	private static IloCplex cplex = null;
        private static IloCplex.Param pcplex = null;
        
	public static IloCplex getCplex() throws IloException {

		if (cplex == null) {
			CPLEXSingleton.cplex = new IloCplex();
			CPLEXSingleton.cplex.setOut(null);
                        
                        CPLEXSingleton.cplex.setParam(IloCplex.Param.Threads, 4);
		}
//                System.out.println("CPX_PARALLEL: " + cplex.getParam(IloCplex.Param.Parallel));
                
		return CPLEXSingleton.cplex;

	}

}
