package VertexCover;


import graph.Graph;
import graph.UndirectedGraph;

import java.util.List;

public enum CompleteNeighbourhoodRule implements ReductionRule {
	
	INSTANCE;

	@Override
	public Pair<Graph, List<String>> apply(Graph graph, int k)
			throws NoKCoverException {
		
		Graph outputGraph = new UndirectedGraph(graph);

		for (String v : graph.vertices()) {

			if (this.neighbourhoodIsClique(v, outputGraph)) {

				List<String> neighbours = outputGraph.neighbours(v);

				outputGraph.removeVertex(v);

				for (String u : neighbours) {
					outputGraph.removeVertex(u);
				}
				
				return new Pair<Graph, List<String>>(outputGraph, neighbours);

			}

		}

		return null;

	}

	private boolean neighbourhoodIsClique(String v, Graph graph) {

		boolean hasAllEdges = true;
		


		for (String u : graph.neighbours(v)) {
			for (String w : graph.neighbours(v)) {
				if (!u.equals(w) && !graph.isAdjacent(u, w)) {
					hasAllEdges = false;
					break;
				}
			}

			if (!hasAllEdges) {
				break;
			}
		}

		return hasAllEdges;
	}

}
