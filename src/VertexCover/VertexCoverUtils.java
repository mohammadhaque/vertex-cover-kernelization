package VertexCover;

import graph.Edge;
import graph.Graph;
import graph.UndirectedGraph;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

public class VertexCoverUtils {

    /**
     * Calculate the vertex-cover for a given graph and k
     */
    public static List<String> ComputeVertexCover(Graph graph, int k) {
        // initialise the list of vertex cover
        List<ReductionRule> rules = new ArrayList<ReductionRule>();
        rules.add(DegreeZeroRule.INSTANCE);
        rules.add(DegreeOneRule.INSTANCE);
        rules.add(DegreeKRule.INSTANCE);
        rules.add(DegreeTwoAdjacentRule.INSTANCE);  // combine {two-vertex}, remove {combined-vertex}U
        rules.add(DegreeTwoNonAdjacentRule.INSTANCE);
        rules.add(CompleteNeighbourhoodRule.INSTANCE);
        rules.add(CrownRule.INSTANCE);
        rules.add(StructionRule.INSTANCE); // combine {two,vertex} add new edge
        rules.add(LPRule.INSTANCE);
        rules.add(GeneralFoldRule.INSTANCE);    // create gf-"new vertex"
//		 Solver solver = BoundedSearchTree.INSTANCE;
        Solver solver = CPLEXSolver.INSTANCE;
//		Solver solver = InterleavingSolver.INSTANCE;

        VertexCoverUtils vc = new VertexCoverUtils();
        List<String> totalCover = new ArrayList<String>();
        try {
            long startTime = System.currentTimeMillis();
            vc.matchingCheck(graph, k);

            Pair<Graph, List<String>> reducedGraphAndCover = null;
            reducedGraphAndCover = vc.reduce(graph, rules, k);

            System.out.println("Reduced Graph: " + reducedGraphAndCover.fst().toString());
            System.out.println("#Vertices: " + reducedGraphAndCover.fst().numVertices() + "\tEdges:" + reducedGraphAndCover.fst().numEdges());

            long reductionTime = System.currentTimeMillis() - startTime;
            List<String> kernelCover = null;
            kernelCover = vc.solve(reducedGraphAndCover.fst(), k - reducedGraphAndCover.snd().size(),
                    solver);
//            System.out.println("Kernel Cover: " + kernelCover.toString());
//            System.out.println("#Vertices: " + kernelCover.size());

            totalCover = new ArrayList<String>();
//            System.out.println("Initial: " + totalCover);

            totalCover.addAll(kernelCover);
//            System.out.println("kernel: " + totalCover);

            totalCover.addAll(reducedGraphAndCover.snd());
//            System.out.println("Reduce: " + totalCover);

            //totalCover.sort(String::compareToIgnoreCase);  //moh: added
            //Collections.sort(totalCover);
            System.out.println(totalCover);
            /*for (String vertex : totalCover) {
                System.out.println(vertex);
            }*/
        } catch (NoKCoverException e) {
            System.err.println("The input graph has no vertex cover of size at most k = " + k + ".");
            System.err.println(e.getMessage());
        }
        return totalCover;
    }

    public static String graphAndSolutionToGMLString(Graph graph,
            List<String> vertexCover) {

        String gml = new String();

        gml += "graph\n[\n";

        List<String> vertices = graph.vertices();
        System.out.println(vertices);
        
        for (int i = 0; i < vertices.size(); i++) {
            String vertex = vertices.get(i);
//            if (vertex.charAt(0) == '\"') {
//                vertex = vertex.substring(1, (vertex.length() - 2));
//            }
//            System.out.println(vertex);
            gml += " node\n [\n" + "  id " + i + "\n  label "
                    + vertex + "\n";

            gml += "  graphics\n  [\n" + "   fill ";
            if (vertexCover.contains(vertices.get(i))) {
                gml += "\"#FF0000\"\n";
            } else {
                gml += "\"#FFFFFF\"\n";
            }

            gml += "   outline \"#000000\"\n  ]\n";

            gml += " ]\n";

        }

        List<Edge> edges = graph.edges();

        for (Edge e : edges) {
            System.out.println(e);
            gml += " edge\n [\n"
                    + "  source " + vertices.indexOf(e.vertexOne()) + "\n"
                    + "  target " + vertices.indexOf(e.vertexTwo()) + "\n ]\n";

        }

        gml += "]";

        return gml;

    }

    public void matchingCheck(Graph graph, int k) throws NoKCoverException {

        List<String> matchedVertices = new ArrayList<String>(graph.numVertices());

        List<Edge> matching = new ArrayList<Edge>();

        for (Edge e : graph.edges()) {

            if (!matchedVertices.contains(e.vertexOne()) && !matchedVertices.contains(e.vertexTwo())) {
                matching.add(e);
                matchedVertices.add(e.vertexOne());
                matchedVertices.add(e.vertexTwo());
            }
        }

        if (matching.size() > k) {
            throw new NoKCoverException("Failed matching test. Minimum possible k is " + matching.size());
        }

    }

    public Pair<Graph, List<String>> reduce(Graph graph, List<ReductionRule> rules, int startingK)
            throws NoKCoverException {

        Graph output = new UndirectedGraph(graph);
        List<String> verticesInCover = new ArrayList<String>();
        boolean graphReduced;
        int k = startingK;
        int round = 1;

        do { // iterativeley running reduction rules untill no more reduction is possible
            graphReduced = false;
//            System.out.println("Reduction Round: " + round);
            //System.out.println("Total reduction Rules: " + rules.size());
            for (ReductionRule rule : rules) {
//                System.out.println(rule.getClass().toString() + " : ");
                Pair<Graph, List<String>> result = rule.apply(output, k);
                if (result != null) {
                    output = result.fst(); // return x=first element from the result Pair<x,y> (where <outputGraph, degreeOneNeighbours> )
                    verticesInCover.addAll(result.snd());   // add all the y=second element fro the result Pair<x,y> (which is degreeOneNeighbours)

//                    System.out.println(result.snd());

                    k -= result.snd().size();   //reduce the value of k by |degreeOneNeighbours|

                    if (k < 0) {
                        throw new NoKCoverException("Added too many vertices in kernelization.");
                    }

                    graphReduced = true;
                    // break;
                }
            }
            round = round + 1;
//            System.out.println("Current Graph size: " + output.numVertices());
        } while (graphReduced);

        return new Pair<Graph, List<String>>(output, verticesInCover);

    }

    public List<String> solve(Graph graph, int startingK, Solver solver) throws NoKCoverException {
        return solver.solve(graph, startingK);
    }

}
