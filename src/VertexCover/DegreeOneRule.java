package VertexCover;



import graph.Graph;
import graph.UndirectedGraph;

import java.util.ArrayList;
import java.util.List;


public enum DegreeOneRule implements ReductionRule {
	
	INSTANCE;

	@Override
	public Pair<Graph, List<String>> apply(Graph graph, int k) throws NoKCoverException {

		Graph outputGraph = new UndirectedGraph(graph);
		int currentK = k;
		
		List<String> degreeOneNeighbours = new ArrayList<String>();
		
		for (String u : graph.vertices()){
			// add teh degree 1 vertex u & it's neighbouring vertex for removal
                        if (graph.degree(u) == 1 && !degreeOneNeighbours.contains(u) && !degreeOneNeighbours.contains(graph.neighbours(u).get(0))){
				degreeOneNeighbours.add(graph.neighbours(u).get(0));			
			}
			
		}
		
		for (String u : degreeOneNeighbours){
			// remove the degree 1 vertex and decrease k
			if (outputGraph.degree(u) > 0){
				outputGraph.removeVertex(u);
				currentK--;
			}
			
		}
		
		if (currentK < 0) throw new NoKCoverException("Added too many vertices in kernelization. (Deg. 1 rule)");
		
		if (degreeOneNeighbours.size() == 0) return null;
		
		return new Pair<Graph, List<String>>(outputGraph, degreeOneNeighbours);
		
	}

}
