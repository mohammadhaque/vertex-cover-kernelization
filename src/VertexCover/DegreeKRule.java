package VertexCover;


import graph.Graph;
import graph.UndirectedGraph;

import java.util.ArrayList;
import java.util.List;


public enum DegreeKRule implements ReductionRule {
	
	INSTANCE;

	@Override
	public Pair<Graph, List<String>> apply(Graph graph, int k) throws NoKCoverException {
		
		Graph outputGraph = new UndirectedGraph(graph);
		List<String> removedVertices = new ArrayList<String>();
		boolean vertexRemoved = false;
		int currentK = k;
		
		for (String v : graph.vertices()){
			
			if (currentK < 0) throw new NoKCoverException("Added too many vertices in kernelization. (Deg. k rule)");
			
			if (outputGraph.degree(v) > k){
				
				outputGraph.removeVertex(v);
				removedVertices.add(v);
				currentK--;
				vertexRemoved = true;
			}
			
		}
		
		if (vertexRemoved){
			return new Pair<Graph, List<String>>(outputGraph, removedVertices);
		}
		
		return null;
	}

}
