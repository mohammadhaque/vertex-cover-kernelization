package VertexCover;


import java.util.List;

import graph.Graph;


public interface ReductionRule{
	
	public Pair<Graph, List<String>> apply(Graph graph, int k) throws NoKCoverException;

}
