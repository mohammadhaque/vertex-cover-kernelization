package VertexCover;



import graph.Graph;
import graph.UndirectedGraph;

import java.util.ArrayList;
import java.util.List;


public enum DegreeTwoNonAdjacentRule implements ReductionRule {
	
	INSTANCE;

	@Override
	public Pair<Graph, List<String>> apply(Graph graph, int k)
			throws NoKCoverException {
		Graph outputGraph = new UndirectedGraph(graph);
		int currentK = k;
		List<String> removedVertices = new ArrayList<String>();
		boolean vertexRemoved = false;
		List<String> verticesMerged = new ArrayList<String>();

		for (String v : graph.vertices()) {

			if (!vertexRemoved && graph.degree(v) == 2 && !verticesMerged.contains(v)) {

				List<String> neighboursOfV = graph.neighbours(v);

				if (!verticesMerged.contains(neighboursOfV.get(0))
						&& !verticesMerged.contains(neighboursOfV.get(1))) {
					if (!graph.isAdjacent(neighboursOfV.get(0),
							neighboursOfV.get(1))) {

						String newVertex = "{" + v + "-" + neighboursOfV.get(0) + "-" + neighboursOfV.get(1) + "}";
												
						outputGraph.addVertex(newVertex);
						
						for (String s : graph.neighbours(neighboursOfV.get(0))){
							outputGraph.addEdge(newVertex, s);
						}
						
						for (String s : graph.neighbours(neighboursOfV.get(1))){
							outputGraph.addEdge(newVertex, s);
						}
						
						removedVertices.add(newVertex + "U");
						
						verticesMerged.add(v);
						verticesMerged.add(neighboursOfV.get(0));
						verticesMerged.add(neighboursOfV.get(1));

						currentK--;
						outputGraph.removeVertex(v);
						outputGraph.removeVertex(neighboursOfV.get(0));
						outputGraph.removeVertex(neighboursOfV.get(1));
						
						vertexRemoved = true;

					}
				}
			}
		}
		
		if (currentK < 0) throw new NoKCoverException("Too many vertices added in kernelization. (Deg. 2 non-adjacent rule)");
		
		if (vertexRemoved){
			return new Pair<Graph, List<String>>(outputGraph, removedVertices);
		}

		return null;
	}

}
