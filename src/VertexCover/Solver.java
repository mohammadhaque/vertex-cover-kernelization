package VertexCover;

import java.util.List;

import graph.Graph;


public interface Solver {
	
	public List<String> solve(Graph graph, int k) throws NoKCoverException;

}
