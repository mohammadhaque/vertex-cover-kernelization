package VertexCover;


import graph.Graph;
import graph.UndirectedGraph;

import java.util.ArrayList;
import java.util.List;

public enum DegreeTwoAdjacentRule implements ReductionRule {
	
	INSTANCE;

	@Override
	public Pair<Graph, List<String>> apply(Graph graph, int k)
			throws NoKCoverException {

		Graph outputGraph = new UndirectedGraph(graph);
		int currentK = k;
		List<String> removedVertices = new ArrayList<String>();
		boolean vertexRemoved = false;

		for (String v : graph.vertices()) {

			if (graph.degree(v) == 2 && !removedVertices.contains(v)) {

				List<String> neighboursOfV = graph.neighbours(v);

				if (!removedVertices.contains(neighboursOfV.get(0))
						&& !removedVertices.contains(neighboursOfV.get(1))) {
					if (graph.isAdjacent(neighboursOfV.get(0),
							neighboursOfV.get(1))) {

						removedVertices.addAll(neighboursOfV);
						currentK -= 2;
						outputGraph.removeVertex(neighboursOfV.get(0));
						outputGraph.removeVertex(neighboursOfV.get(1));
						vertexRemoved = true;
					}
				}
			}
		}
		
		if (currentK < 0) throw new NoKCoverException("Too many vertices added in kernelization. (Deg. 2 adjacent rule)");
		
		if (vertexRemoved){
			return new Pair<Graph, List<String>>(outputGraph, removedVertices);
		}

		return null;
	}

}
