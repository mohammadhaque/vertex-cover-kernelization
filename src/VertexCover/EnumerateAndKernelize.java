package VertexCover;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import graph.Edge;
import graph.Graph;
import graph.UndirectedGraph;
import graph.UndirectedMatrixGraph;

public class EnumerateAndKernelize {

	private static int size = 8;
	private static String graphSourceFile = "graph" + size + ".g6";

	public static int findK(Graph graph, VertexCoverUtils vcSolver,
			List<ReductionRule> rules, Solver solver) {

		int k = 0;

		boolean kFound = false;

		while (!kFound) {

			try {
				Pair<Graph, List<String>> reducedInstance = vcSolver.reduce(
						graph, rules, k);
				vcSolver.solve(reducedInstance.fst(), k
						- reducedInstance.snd().size(), solver);
				kFound = true;
			} catch (NoKCoverException e) {
				k++;
			}

		}

		return k;
	}

	public static void main(String[] args) throws NoKCoverException,
			IOException {

		File graphs = new File(graphSourceFile);

		BufferedReader reader = new BufferedReader(new FileReader(graphs));

		List<String> allMasks = new ArrayList<String>();

		while (reader.ready()) {
			String graph = reader.readLine();
			graph = graph.substring(1);
			String mask = "";
			for (int i = 0; i < graph.length(); i++) {
				mask += Integer.toBinaryString(graph.charAt(i) - 63);
			}

			while (mask.length() < size * (size - 1) / 2) {
				mask = "0" + mask;
			}

			if (mask.length() > size * (size - 1) / 2) {
				mask = mask.substring(0, size * (size - 1) / 2);
			}

			allMasks.add(mask);
		}

		Graph baseGraph = new UndirectedMatrixGraph();

		VertexCoverUtils vc = new VertexCoverUtils();
		List<ReductionRule> rules = new ArrayList<ReductionRule>();

		rules.add(DegreeZeroRule.INSTANCE);
		rules.add(DegreeOneRule.INSTANCE);
		rules.add(DegreeKRule.INSTANCE);
		rules.add(DegreeTwoAdjacentRule.INSTANCE);
		rules.add(DegreeTwoNonAdjacentRule.INSTANCE);
		rules.add(CompleteNeighbourhoodRule.INSTANCE);
		rules.add(CrownRule.INSTANCE);
		rules.add(StructionRule.INSTANCE);
		rules.add(LPRule.INSTANCE);
		rules.add(GeneralFoldRule.INSTANCE);

		Solver solver = CPLEXSolver.INSTANCE;

		for (int i = 0; i < EnumerateAndKernelize.size; i++) {
			baseGraph.addVertex("v" + i);
		}

		List<Edge> possibleEdges = baseGraph.nonEdges();

		for (String mask : allMasks) {

			Graph currentGraph = new UndirectedGraph(baseGraph);

			boolean[][] matrix = new boolean[size][size];

			for (int i = 0; i < size; i++) {
				for (int j = i + 1; j < size; j++) {
					if (mask.charAt(mask.length() - (i + j)) == '1') {
						matrix[i][j] = true;
						matrix[j][i] = true;
					}
				}
			}

			currentGraph.setAdjacencyMatrix(matrix);
			
			int k = findK(currentGraph, vc, rules, solver);
			Pair<Graph, List<String>> reduced = vc.reduce(currentGraph, rules,
					k);

			if (!(reduced.fst().numVertices() < currentGraph.numVertices())) {
				System.out.println("NOT REDUCED: " + currentGraph);
			}

		}

		reader.close();
		System.out.println("Finished.");

	}
}
