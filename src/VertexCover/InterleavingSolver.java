package VertexCover;

import java.util.ArrayList;
import java.util.List;

import graph.Graph;
import graph.UndirectedGraph;

public enum InterleavingSolver implements Solver {
	INSTANCE;

	@Override
	public List<String> solve(Graph graph, int startingK) throws NoKCoverException {
		
		System.gc();

		int k = startingK;
		
		List<ReductionRule> rules = new ArrayList<ReductionRule>();

		rules.add(DegreeZeroRule.INSTANCE);
		rules.add(DegreeOneRule.INSTANCE);
		rules.add(DegreeKRule.INSTANCE);
		rules.add(DegreeTwoAdjacentRule.INSTANCE);
		rules.add(DegreeTwoNonAdjacentRule.INSTANCE);
		rules.add(CompleteNeighbourhoodRule.INSTANCE);
		rules.add(CrownRule.INSTANCE);
		rules.add(StructionRule.INSTANCE);
		rules.add(LPRule.INSTANCE);
		rules.add(GeneralFoldRule.INSTANCE);

		if (k < 0)
			throw new NoKCoverException("Kernel too large for current k value.");

		if (graph.numEdges() == 0)
			return new ArrayList<String>();

		Graph reductionOutput = new UndirectedGraph(graph);
		List<String> verticesInCover = new ArrayList<String>();
		boolean graphReduced;

		do {
			graphReduced = false;

			for (ReductionRule rule : rules) {
				Pair<Graph, List<String>> result = rule.apply(reductionOutput, k);
				if (result != null) {
					reductionOutput = result.fst();
					verticesInCover.addAll(result.snd());
					k -= result.snd().size();

					if (k < 0) {
						throw new NoKCoverException("Added too many vertices in kernelization.");
					}

					graphReduced = true;
					// break;
				}
			}
		} while (graphReduced);
		
		if (reductionOutput.numEdges() == 0)
			return verticesInCover;
		
		for (String v : reductionOutput.vertices()){
			
			if (reductionOutput.degree(v) == 0) continue;
			
			List<String> neighboursOfV = reductionOutput.neighbours(v);
			
			try{
				
				Graph withoutNeighbours = new UndirectedGraph(reductionOutput);
				for (String u : neighboursOfV){
					withoutNeighbours.removeVertex(u);
				}
				verticesInCover.addAll(this.solve(withoutNeighbours, k - neighboursOfV.size()));
				verticesInCover.addAll(neighboursOfV);
				return verticesInCover;
			}
			catch(NoKCoverException e){
				
				Graph withoutV = new UndirectedGraph(reductionOutput);
				withoutV.removeVertex(v);
				verticesInCover.addAll(this.solve(withoutV, k - 1));
				verticesInCover.add(v);
				return verticesInCover;
				
			}
			
		}

		return null;
	}

}
