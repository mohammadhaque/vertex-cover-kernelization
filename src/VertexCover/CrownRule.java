package VertexCover;



import graph.Edge;
import graph.Graph;
import graph.UndirectedEdge;
import graph.UndirectedGraph;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Queue;

/**
 * An implementation of the Crown Rule reduction for
 * Vertex Cover. This is not the most optimised version,
 * as the procedure for finding augmenting paths is not
 * optimal.
 * 
 * The apply method is the basic external entry point.
 * 
 * The method either finds a Crown, and returns the
 * reduced graph and the vertices to add to the cover,
 * null if no change has been made, or throws a NoKCoverException
 * if the crown found is too large (i.e. k is too small).
 * 
 * @author Luke Mathieson
 *
 */
public enum CrownRule implements ReductionRule {
	
	INSTANCE;

	/**
	 * Central, public method of the class.
	 * 
	 * @param graph
	 * @param k
	 * @return
	 * 
	 * @throws NoKCoverException
	 */
	@Override
	public Pair<Graph, List<String>> apply(Graph graph, int k)
			throws NoKCoverException {

		/*
		 * Compute a matching, the set of unmatched vertices
		 * forms the set of outsiders which the independent set
		 * in the crown will be drawn from.
		 */
		List<String> matchedVertices = new ArrayList<String>(
				graph.numVertices());

		for (Edge e : graph.edges()) {

			if (!matchedVertices.contains(e.vertexOne())
					&& !matchedVertices.contains(e.vertexTwo())) {
				matchedVertices.add(e.vertexOne());
				matchedVertices.add(e.vertexTwo());
			}

		}

		List<String> outsiders = new ArrayList<String>(graph.vertices());
		List<String> neighbours = new ArrayList<String>();

		for (String v : matchedVertices) {
			outsiders.remove(v);
		}

		/*
		 * Construct the auxiliary graph from which we will
		 * compute a maximum matching to either form the core
		 * of the crown, or provide the basis to iterate to
		 * find one.
		 */
		Graph auxiliaryGraph = new UndirectedGraph();

		for (String v : outsiders) {
			auxiliaryGraph.addVertex(v);
			for (String u : graph.neighbours(v)) {
				auxiliaryGraph.addVertex(u);
				auxiliaryGraph.addEdge(u, v);
				if (!neighbours.contains(u)) {
					neighbours.add(u);
				}
			}
		}

		/*
		 * Look for an augmenting path to increase the size of
		 * the matching. This is the simple way, where we just
		 * look for a single augmenting path. There is a more
		 * sophisticated method that computes a larger set of 
		 * disjoint paths, so change this if you want better
		 * overall running time performance.
		 */
		List<Edge> currentMatching = new ArrayList<Edge>();
		List<Edge> augmentingPath = new ArrayList<Edge>();

		do {

			augmentingPath = this.findAugmentingPath(auxiliaryGraph,
					currentMatching);
			currentMatching = this.symmetricDifference(currentMatching,
					augmentingPath);

		} while (augmentingPath.size() > 0);
		
		boolean neighboursMatched = true;
		
		/*
		 * Check whether the prospective head has been completely
		 * matched. If it has, we can stop and return the crown
		 * where I = outsiders and H = neighbours.
		 */
		for (String v : neighbours){
			if (!this.inEdgeSet(currentMatching, v)) neighboursMatched = false;
		}
	
		if (neighboursMatched){
			
			Graph outputGraph = new UndirectedGraph(graph);
			
			for (String v : outsiders) outputGraph.removeVertex(v);
			for (String v : neighbours) outputGraph.removeVertex(v);
			
			if (neighbours.size() > k)  throw new NoKCoverException("Added too many vertices in kernelization. (Crown rule)");
			
			if (neighbours.size() > 0)	return new Pair<Graph, List<String>>(outputGraph, neighbours);
			return null;
			
		}

		/*
		 * If the secondary matching doesn't produce
		 * a crown, we can look at the unmatched vertices
		 * from the outsiders, and iteratively build up
		 * I and H with those vertices as a starting point.
		 *
		 * I_0 = unmatched outsider vertices (from auxiliary matching).
		 * 
		 * Repeat the following until I_n = I_n-1:
		 * 	H_n = N(I_n)
		 * 	I_n+1 = I_n ⋃ N_currentMatching(H_n)
		 * 
		 * When this stops, I = I_n and H = H_n.
		 *
		 */
		List<String> I = new ArrayList<String>();

		for (String v : outsiders) {
			if (!this.inEdgeSet(currentMatching, v))
				I.add(v);
		}
		
		if (I.size() == 0) return null;

		List<String> H = new ArrayList<String>();

		while (true) {

			List<String> newI = new ArrayList<String>();

			for (String v : I) {
				for (String u : neighbours) {
					if (!H.contains(u) && auxiliaryGraph.isAdjacent(u, v))
						H.add(u);
				}
			}

			newI.addAll(I);

			for (String v : H) {
				if (this.inEdgeSet(currentMatching, v)) {
					String u = this.matchedVertex(currentMatching, v);
					if (!newI.contains(u)) {
						newI.add(u);
					}
				}
			}


			if (this.equalSets(I, newI)) {
				break;
			} else {
				I = new ArrayList<String>(newI);
				H = new ArrayList<String>();
			}

		}

		Graph outputGraph = new UndirectedGraph(graph);

		for (String v : H) {
			outputGraph.removeVertex(v);
		}

		for (String v : I) {
			outputGraph.removeVertex(v);
		}

		if (H.size() > k) throw new NoKCoverException("Added too many vertices in kernelization. (Crown rule)");
		
		if (H.size() > 0) {

			return new Pair<Graph, List<String>>(outputGraph,
					new ArrayList<String>(H));

		}

		return null;
	}

	/**
	 * Given a matching and a vertex v, returns the neighbour
	 * of v in the matching. If no such vertex exists, then 
	 * null is returned.
	 * 
	 * @param matching
	 * @param v
	 * @return
	 */
	private String matchedVertex(List<Edge> matching, String v) {

		for (Edge e : matching) {
			if (v.equals(e.vertexOne()))
				return e.vertexTwo();

			if (v.equals(e.vertexTwo()))
				return e.vertexOne();
		}

		return null;
	}

	/**
	 * Compares two collections as sets. If they
	 * contain precisely the same elements (ignoring
	 * duplicates), the method returns true. Otherwise
	 * false is returned.
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	private <T> boolean equalSets(Collection<T> a, Collection<T> b) {

		if (a.size() != b.size())
			return false;

		for (T t : a) {
			if (!b.contains(t))
				return false;
		}

		for (T t : b) {
			if (!a.contains(t))
				return false;
		}

		return true;
	}

	/**
	 * Takes two lists and returns a list of the elements
	 * that appear in  exactly one of the input lists.
	 *  
	 * @param listOne
	 * @param listTwo
	 * @return
	 */
	private <T> List<T> symmetricDifference(List<? extends T> listOne,
			List<? extends T> listTwo) {

		List<T> difference = new ArrayList<T>();

		for (T e : listOne) {
			if (!listTwo.contains(e))
				difference.add(e);
		}

		for (T e : listTwo) {
			if (!listOne.contains(e))
				difference.add(e);
		}

		return difference;
	}

	/**
	 * Finds an augmenting path in the given graph.
	 * 
	 * @param auxiliaryGraph
	 * @param currentMatching
	 * @return
	 */
	private List<Edge> findAugmentingPath(Graph auxiliaryGraph,
			List<Edge> currentMatching) {

		List<String> matchedVertices = new ArrayList<String>(
				currentMatching.size() * 2);

		for (Edge e : currentMatching) {
			matchedVertices.add(e.vertexOne());
			matchedVertices.add(e.vertexTwo());
		}

		List<String> unmatchedVertices = new ArrayList<String>(
				auxiliaryGraph.vertices());
		for (String v : matchedVertices) {
			unmatchedVertices.remove(v);
		}

		for (String v : unmatchedVertices) {

			List<Edge> currentValidPath = new ArrayList<Edge>();

			Queue<PathHead> BFSQueue = new ArrayDeque<PathHead>();

			BFSQueue.add(new PathHead(v, true, new ArrayList<Edge>(
					currentValidPath)));

			while (!BFSQueue.isEmpty()) {

				PathHead pathHead = BFSQueue.poll();

				for (String u : auxiliaryGraph.neighbours(pathHead
						.getHeadVertex())) {

					if (!this.inEdgeSet(pathHead.getPartialPath(), u)) {

						if (pathHead.isLookingForFreeEdge()
								&& !currentMatching
										.contains(new UndirectedEdge(pathHead
												.getHeadVertex(), u))) {

							List<Edge> extendedPath = new ArrayList<Edge>(
									pathHead.getPartialPath());
							Edge newEdge = new UndirectedEdge(
									pathHead.getHeadVertex(), u);
							extendedPath.add(newEdge);

							if (unmatchedVertices.contains(u)) {
								return extendedPath;
							}

							BFSQueue.offer(new PathHead(u, false, extendedPath));

						} else if (!pathHead.isLookingForFreeEdge()
								&& currentMatching.contains(new UndirectedEdge(
										pathHead.getHeadVertex(), u))) {

							List<Edge> extendedPath = new ArrayList<Edge>(
									pathHead.getPartialPath());
							Edge newEdge = new UndirectedEdge(
									pathHead.getHeadVertex(), u);
							extendedPath.add(newEdge);

							BFSQueue.offer(new PathHead(u, true, extendedPath));

						}

					}

				}

			}

		}

		return Collections.<Edge> emptyList();
	}

	private boolean inEdgeSet(List<Edge> edgeSet, String u) {

		for (Edge e : edgeSet) {
			if (u.equals(e.vertexOne()) || u.equals(e.vertexTwo()))
				return true;
		}

		return false;
	}

}

/**
 * Helper class that stores the current state of the
 * path being built during the BFS when finding an
 * augmenting path. Really just a wrapper to save
 * unnecessary typing and keep relevant data together.
 * 
 * @author Luke Mathieson
 *
 */
class PathHead {

	String headVertex;
	boolean lookingForFreeEdge;
	List<Edge> partialPath;

	public PathHead(String headVertex, boolean lookingForFreeEdge,
			List<Edge> partialPath) {
		super();
		this.headVertex = headVertex;
		this.lookingForFreeEdge = lookingForFreeEdge;
		this.partialPath = partialPath;
	}

	public String getHeadVertex() {
		return headVertex;
	}

	public boolean isLookingForFreeEdge() {
		return lookingForFreeEdge;
	}

	public List<Edge> getPartialPath() {
		return partialPath;
	}
}
