package VertexCover;



import graph.Graph;
import graph.UndirectedGraph;

import java.util.ArrayList;
import java.util.List;

public enum DegreeZeroRule implements ReductionRule {
	
	INSTANCE;

	@Override
	public Pair<Graph, List<String>> apply(Graph graph, int k) {

		Graph outputGraph = new UndirectedGraph(graph);
		boolean removedSomething = false;

		for (String u : graph.vertices()) {
			if (graph.degree(u) == 0) {
				outputGraph.removeVertex(u);
				removedSomething = true;
			}
		}

		if (!removedSomething)
			return null;

		return new Pair<Graph, List<String>>(outputGraph,
				new ArrayList<String>());

	}

}
