package VertexCover;



import graph.Graph;
import graph.Edge;
import graph.UndirectedEdge;
import graph.UndirectedGraph;

import java.util.ArrayList;
import java.util.List;

public enum StructionRule implements ReductionRule {
	
	INSTANCE;

	public Pair<Graph, List<String>> apply(Graph graph, int k)
			throws NoKCoverException {
		
		List<String> vertices = graph.vertices();

		for (String v : vertices) {

			Pair<Graph, List<String>> output = this.applyAtVertex(graph, v, k);

			if (output != null)
				return output;

		}

		return null;

	}

	public Pair<Graph, List<String>> applyAtVertex(Graph graph, String v, int k)
			throws NoKCoverException {

		Graph outputGraph = new UndirectedGraph(graph);

		List<String> neighbours = graph.neighbours(v);

		List<Edge> missingEdges = new ArrayList<Edge>();

		for (String u : neighbours) {
			for (String w : neighbours) {

				if (!u.equals(w) && !graph.isAdjacent(u, w)) {

					Edge e = new UndirectedEdge(u, w);
					if (!missingEdges.contains(e)) {
						missingEdges.add(e);
					}
				}

			}
		}

		if (missingEdges.size() < neighbours.size()) {

			int addedCount = 0;

			List<String> newVertices = new ArrayList<String>();

			for (Edge e : missingEdges) {
				String newVertex = "{" + e.vertexOne() + "," + e.vertexTwo()
						+ "}";
				outputGraph.addVertex(newVertex);
				addedCount++;

				for (String n : newVertices)
					outputGraph.addEdge(n, newVertex);

				newVertices.add(newVertex);

				for (String x : graph.neighbours(e.vertexOne())) {
					outputGraph.addEdge(x, newVertex);
				}

				for (String x : graph.neighbours(e.vertexTwo())) {
					outputGraph.addEdge(x, newVertex);
				}

			}

			for (String u : neighbours) {
				outputGraph.removeVertex(u);
			}

			outputGraph.removeVertex(v);

			List<String> solutionVertices = new ArrayList<String>();

			for (int i = 0; i < neighbours.size() - addedCount; i++) {
				solutionVertices.add(new String("n-" + v + "-" + i));
			}

			if (solutionVertices.size() > k)
				throw new NoKCoverException(
						"Added too many vertices in kernelization (Struction rule).");

			return new Pair<Graph, List<String>>(outputGraph, solutionVertices);

		}

		return null;
	}
}
