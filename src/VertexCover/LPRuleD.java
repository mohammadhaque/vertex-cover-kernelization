package VertexCover;



import graph.Edge;
import graph.Graph;
import graph.UndirectedGraph;

import ilog.concert.IloException;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;

import java.util.ArrayList;
import java.util.List;

public enum LPRuleD implements ReductionRule {
	
	INSTANCE;

	//The acceptable error in the doubles.
	private static final double EPSILON = 0.000001;

	@Override
	public Pair<Graph, List<String>> apply(Graph graph, int k)
			throws NoKCoverException {

		// Obtain a canonical version for the indices of the vertices.
		List<String> vertices = graph.vertices();
                System.out.println("\nReduction Rule Output in Detail: LP Rule");
		try {
			IloCplex cplex = CPLEXSingleton.getCplex();
			List<IloNumVar> variables = new ArrayList<IloNumVar>(vertices.size());
			
			for (int i = 0; i < vertices.size(); i++){
				System.out.println(vertices.get(i));
                                        
				variables.add(cplex.numVar(0, 1, vertices.get(i)));
				
			}
			
			IloNumVar[] varArray = variables.toArray(new IloNumVar[variables.size()]);
			
			//IloRange[] constraints = new IloRange[graph.numEdges()];
			
			List<Edge> edges = graph.edges();
			
			for (int i = 0; i < edges.size(); i++){
                            String u = edges.get(i).vertexOne();
                            String v = edges.get(i).vertexTwo();

				//System.out.println(u + ", " + v + " : " + varArray[vertices.indexOf(u)] + ", " + varArray[vertices.indexOf(v)]);
				/*constraints[i] =*/ cplex.addGe(cplex.sum(varArray[vertices.indexOf(edges.get(i).vertexOne())], varArray[vertices.indexOf(edges.get(i).vertexTwo())]), 1);
				
			}
			
			cplex.addMinimize(cplex.sum(varArray));
			
			if (cplex.solve()){

			    double[] values = cplex.getValues(varArray);
			    
			    List<String> P = new ArrayList<String>();
			    
			    List<String> R = new ArrayList<String>();

                             for (int i = 0; i < values.length; i++){
			    	System.out.println(vertices.get(i) + " : " + values[i]);
			    	if (values[i] > 0.5 + LPRuleD.EPSILON) P.add(vertices.get(i));
			    	if (values[i] < 0.5 - LPRuleD.EPSILON) R.add(vertices.get(i));
                                
                                //if(values[i] > 0.5) System.out.println(vertices.get(i) + " : " + values[i]);
			    	
			    }
			    
                            System.out.println("P = " + P.toString());
                            System.out.println("R = " + R.toString());
                            
			
			    cplex.clearModel();
			    
			    if (P.size() > k) throw new NoKCoverException("Too many vertices added in kernelization (LP Rule).");
			    

			    if (P.size() == 0 && R.size() == 0) {
                            
                                return null;
                            }
			    
			    Graph outputGraph = new UndirectedGraph(graph);

			    for (String v : P) outputGraph.removeVertex(v);
			    for (String v : R) outputGraph.removeVertex(v);
			    
                            System.out.println("Vertices in P:" + P);
                            System.out.println("Vertices in R:" + R);
                            
			    return new Pair<Graph, List<String>>(outputGraph, P);

			}
			
			cplex.clearModel();
			
		} catch (IloException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
}
