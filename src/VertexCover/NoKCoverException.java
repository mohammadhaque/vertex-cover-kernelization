package VertexCover;


@SuppressWarnings("serial")
public class NoKCoverException extends Exception {

	public NoKCoverException(String msg) {
		super(msg);
	}

	public NoKCoverException() {
		super();
	}

}
