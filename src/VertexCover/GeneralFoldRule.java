package VertexCover;


import graph.Edge;
import graph.Graph;
import graph.UndirectedGraph;

import ilog.concert.IloException;
import ilog.concert.IloNumVar;
import ilog.concert.IloRange;
import ilog.cplex.IloCplex;

import java.util.ArrayList;
import java.util.List;

/**
 * Implements the General-Fold rule from Chen, Kanj & Xia (2010).
 * 
 * This requires an implementation of the Nemhauser-Trotter LP based reduction.
 * 
 * In this version, we rely on the reduction structure outside this class to
 * actually apply the NT LP reduction. Here it is only used to check whether
 * this rule can be applied, then inside the fold operation itself.
 * 
 * @author Luke Mathieson
 * 
 */
public enum GeneralFoldRule implements ReductionRule {
	
	INSTANCE;

	private static final double EPSILON = 0.000001;

	@Override
	public Pair<Graph, List<String>> apply(Graph graph, int k)
			throws NoKCoverException {

		LPRule lpReduction = LPRule.INSTANCE;

		Pair<Graph, List<String>> NToutput = lpReduction.apply(graph, k);

		if (NToutput == null) {

			for (String v : graph.vertices()) {
				Graph testGraph = new UndirectedGraph(graph);
				testGraph.removeVertex(v);

				try {
					IloCplex cplex = CPLEXSingleton.getCplex();
					List<IloNumVar> variables = new ArrayList<IloNumVar>(
							testGraph.vertices().size());

					for (int i = 0; i < testGraph.vertices().size(); i++) {

						variables.add(cplex.numVar(0, 1, testGraph.vertices()
								.get(i)));

					}

					IloNumVar[] varArray = variables
							.toArray(new IloNumVar[variables.size()]);

					IloRange[] constraints = new IloRange[testGraph.numEdges()];

					List<Edge> edges = testGraph.edges();

					for (int i = 0; i < edges.size(); i++) {

						constraints[i] = cplex.addGe(cplex.sum(
								varArray[testGraph.vertices().indexOf(
										edges.get(i).vertexOne())],
								varArray[testGraph.vertices().indexOf(
										edges.get(i).vertexTwo())]), 1);

					}

					cplex.addMinimize(cplex.sum(varArray));

					if (cplex.solve()) {

						double[] values = cplex.getValues(varArray);

						List<String> P = new ArrayList<String>();
						List<String> Q = new ArrayList<String>();
						List<String> R = new ArrayList<String>();
						


						for (int i = 0; i < values.length; i++) {

							if (values[i] > 0.5 + GeneralFoldRule.EPSILON)
								P.add(testGraph.vertices().get(i));
							else if (values[i] < 0.5 - GeneralFoldRule.EPSILON)
								R.add(testGraph.vertices().get(i));
							else
								Q.add(testGraph.vertices().get(i));

						}
						
						cplex.clearModel();
						
						if (P.size() > 0){
							
							List<String> largeHead = new ArrayList<String>(P);
							largeHead.add(v);
							
							Graph outputGraph = new UndirectedGraph(graph);
							
							if (this.isIndependentSet(graph, largeHead)){
								
								String newVertex = "gf-" + R.toString();
								outputGraph.addVertex(newVertex);
								
								for (String u : largeHead){
									for (String w : graph.neighbours(u)){
										outputGraph.addEdge(newVertex, w);
									}
									
									outputGraph.removeVertex(u);
								}
								
								for (String u : R){
									outputGraph.removeVertex(u);
								}
								
								return new Pair<Graph, List<String>>(outputGraph, R);
								
							}
							else{
								
								for (String u : R){
									outputGraph.removeVertex(u);
								}
								
								for (String u : largeHead){
									outputGraph.removeVertex(u);
								}
								
								return new Pair<Graph, List<String>>(outputGraph, largeHead);
								
							}
							
						}

					}
					
					cplex.clearModel();

				} catch (IloException e) {

				}

			}

		}

		return null;
	}

	private boolean isIndependentSet(Graph graph, List<String> vertexSet) {

		for (String u : vertexSet) {
			for (String v : vertexSet) {
				if (graph.isAdjacent(u, v)) {
					return false;
				}
			}
		}

		return true;
	}

}
