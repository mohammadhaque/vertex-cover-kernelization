/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VertexCover;

import graph.Graph;
import java.util.List;

/**
 *
 * @author mnh998
 */
public interface VertexCover {
    public List<String>  ComputeVertexCover( Graph graph, int k);
    
}
