package VertexCover;

import graph.Edge;
import graph.Graph;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloRange;
import ilog.cplex.IloCplex;

import java.util.ArrayList;
import java.util.List;

public enum CPLEXSolver implements Solver {
	
	INSTANCE;

	@Override
	public List<String> solve(Graph graph, int k) throws NoKCoverException {

		try {
			IloCplex cplex = CPLEXSingleton.getCplex();

			List<String> variableNames = graph.vertices();
			List<IloIntVar> variables = new ArrayList<IloIntVar>(
					variableNames.size());

			for (int i = 0; i < variableNames.size(); i++) {

				variables.add(cplex.intVar(0, 1, variableNames.get(i)));

			}

			IloIntVar[] varArray = variables.toArray(new IloIntVar[variables
					.size()]);

			IloRange[] constraints = new IloRange[graph.numEdges()];

			List<Edge> edges = graph.edges();

			for (int i = 0; i < edges.size(); i++) {

				constraints[i] = cplex.addGe(cplex.sum(varArray[variableNames
						.indexOf(edges.get(i).vertexOne())],
						varArray[variableNames
								.indexOf(edges.get(i).vertexTwo())]), 1);

			}

			cplex.addMinimize(cplex.sum(varArray));

			List<String> solution = null;

			if (cplex.solve()) {

				double[] values = cplex.getValues(varArray);

				solution = new ArrayList<String>();

				for (int i = 0; i < varArray.length; i++) {

					if (1.0 - values[i] < 0.00000001) {

						solution.add(variableNames.get(i));

					}

				}

			}
			
			cplex.clearModel();
			
			if (solution != null && solution.size() > k) throw new NoKCoverException();

			return solution;

		} catch (IloException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

}
