package solvers;
import graph.Edge;
import graph.Graph;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.cplex.IloCplex;
import VertexCover.CPLEXSingleton;

import java.util.ArrayList;
import java.util.List;


public enum CPLEXExactSolver implements Solver {
    INSTANCE;
    final double THRESHOLD = 0.00000001;
    @Override
    public List<String> solve(Graph graph) {
        try {
            IloCplex cplex = CPLEXSingleton.getCplex();

            List<String> vertexNames = graph.vertices();
            List<IloIntVar> variables = new ArrayList<IloIntVar>(vertexNames.size());

            // Modeling Variables : add LP variables in CPLEX
            for (int i = 0; i < vertexNames.size(); i++) {
                variables.add(cplex.intVar(0, 1, vertexNames.get(i)));  // X_u \in {0,1} for all u \in V
            }

            // Ranged Constraints : creating constratints
            IloIntVar[] varArray = variables.toArray(new IloIntVar[variables.size()]);
            //IloRange[] constraints = new IloRange[graph.numEdges()];
            List<Edge> edges = graph.edges();
            // create constraint for each edges as : X_u + X_v >= 1
            for (int i = 0; i < edges.size(); i++) {
                IloIntVar vertexU = varArray[vertexNames.indexOf(edges.get(i).vertexOne())];
                IloIntVar vertexV = varArray[vertexNames.indexOf(edges.get(i).vertexTwo())];
                cplex.addGe(cplex.sum(vertexU,vertexV), 1);    //add constraint X_u + X_v >= 1
            }

            // Objective Function : create the LP Model : min_{v \in V}{ X_v }
            cplex.addMinimize(cplex.sum(varArray));
            
            //Solve the LP Model
            List<String> solution = null;
            if (cplex.solve()) {    // extract the result
                double[] values = cplex.getValues(varArray);
                solution = new ArrayList<String>();
//                System.out.println("Solution:");
                for (int i = 0; i < varArray.length; i++) {
//                    System.out.println(varArray[i] + " : " + values[i]);
                    if ( Math.abs(1.0 - values[i]) < THRESHOLD) {
                        solution.add(vertexNames.get(i));
                    }
                }
            }
            cplex.clearModel();
            return solution;
        
        } catch (IloException e) {
            e.printStackTrace();
        }
        return null;
    }

}
